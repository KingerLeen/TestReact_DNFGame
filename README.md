### 项目启动

* 基础构建
```
1 确定安装了 nodejs & yarn
    yarn -v / yarn --version
    nodejs -v / nodejs --version
2 进入项目根目录
    cd *
3 安装所有依赖
    yarn install
```

* 启动服务
```
1 进入项目根目录
    cd *
2 启动服务端(127.0.0.1:3000)
    node ./server/server.js
3 启动客户端(127.0.0.1:2333)
    yarn start
4 网页地址
    http://127.0.0.1:3000/index.html
```

* 备注
```
1 json数据库  （ @ _ @ ）
    ./server/info.json
2 程序控制，难度调整
    ./src/func,js
```